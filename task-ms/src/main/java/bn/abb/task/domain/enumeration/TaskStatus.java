package bn.abb.task.domain.enumeration;

public enum TaskStatus {

    FINISHED, ONGOING, DRAFT
}
