package bn.abb.task.web.rest;

import bn.abb.task.dto.token.InteractionResponse;
import bn.abb.task.dto.user.LoginCreateRequestDto;
import bn.abb.task.dto.user.RegisterRequestDto;
import bn.abb.task.service.impl.AuthServiceImpl;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {


    private AuthServiceImpl authService;

    public AuthController(AuthServiceImpl authService) {
        this.authService = authService;
    }


    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody RegisterRequestDto regRequest) {
        authService.registerUser(regRequest);
        return ResponseEntity.ok(new InteractionResponse("User registered successfully!"));
    }


    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody LoginCreateRequestDto loginRequest) {
        return authService.login(loginRequest);
    }
}
