package bn.abb.task.config;



import bn.abb.common.config.ModelMapperConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ ModelMapperConfig.class})
public class CommonConfig {

}
