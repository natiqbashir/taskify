package bn.abb.task.dto.task;


import bn.abb.task.domain.enumeration.TaskStatus;
import bn.abb.task.dto.organization.OrganizationResponseInfoDto;
import bn.abb.task.dto.user.UserDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskResponseDto {

    private Long id;
    private String title;
    private String description;
    private TaskStatus status;
    private LocalDateTime deadline;
    private OrganizationResponseInfoDto organization;
    private List<UserDto> assignee;
}

