package bn.abb.task.dto.task;


import bn.abb.task.domain.enumeration.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskCreateRequestDto {

    @NotBlank
    private String title;

    @NotBlank
    private String description;

    @Valid
    private TaskStatus status;

    private LocalDateTime deadline;

    private Long organizationId;


}