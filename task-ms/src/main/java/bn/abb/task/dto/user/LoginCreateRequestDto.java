package bn.abb.task.dto.user;


import bn.abb.task.service.validation.ValidPassword;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@Builder
public class LoginCreateRequestDto {
    @NotBlank
    private String email;

    @NotBlank
    @ValidPassword
    private String password;

}