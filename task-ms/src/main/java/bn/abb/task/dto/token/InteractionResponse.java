package bn.abb.task.dto.token;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;


@Data
@AllArgsConstructor
public class InteractionResponse {

    private String inter;

}