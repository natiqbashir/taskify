package bn.abb.task.service.impl;


import bn.abb.common.exception.NotFoundException;
import bn.abb.task.domain.Authority;
import bn.abb.task.domain.User;
import bn.abb.task.domain.enumeration.AuthEnum;
import bn.abb.task.dto.user.RegisterUserDto;
import bn.abb.task.dto.user.UserDto;
import bn.abb.task.exception.EmailAlreadyUsedException;
import bn.abb.task.exception.UserNotFoundException;
import bn.abb.task.mapper.UserMapper;
import bn.abb.task.repository.AuthorityRepository;
import bn.abb.task.repository.UserRepository;
import bn.abb.task.security.SecurityService;
import bn.abb.task.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final AuthorityRepository authRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;
    private final SecurityService securityService;


    @Override
    public void create(RegisterUserDto dto) {
        userRepository.findByEmail(dto.getEmail())
                .ifPresent(user -> {
                    throw new EmailAlreadyUsedException(dto.getEmail());
                });
        User user = createUserEntityObject(dto);
        userRepository.save(user);
    }


    private User createUserEntityObject(RegisterUserDto registerUserDto) {
        User user = userMapper.dtoToEntity(registerUserDto);
        user.setPassword(passwordEncoder.encode(registerUserDto.getPassword()));
        Set<Authority> authEntities = new HashSet<>();
        Authority userAuth = authRepository.findByName(AuthEnum.USER)
                .orElseThrow(() -> new NotFoundException("Not found Authority"));
        authEntities.add(userAuth);
        user.setAuthorities(authEntities);
        return user;
    }

    @Override
    public UserDto getCurrentUser() {
        return userMapper.entityToDto(getCurrentUserByEmail(getCurrentLogin()));
    }


    private User getCurrentUserByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(UserNotFoundException::new);
    }

    private String getCurrentLogin() {
        return securityService.getCurrentUserLogin().orElseThrow(UserNotFoundException::new);
    }
}

