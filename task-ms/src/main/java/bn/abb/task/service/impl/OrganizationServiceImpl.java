package bn.abb.task.service.impl;

import bn.abb.task.domain.Organization;
import bn.abb.task.domain.User;
import bn.abb.task.dto.organization.AddUserToOrgDto;
import bn.abb.task.dto.organization.OrganizationCreateRequestDto;
import bn.abb.task.dto.organization.OrganizationResponseInfoDto;
import bn.abb.task.exception.UserNotFoundException;
import bn.abb.task.exception.NameMustBeUniqueException;
import bn.abb.task.exception.organization.OrganizationAlreadyExistException;
import bn.abb.task.exception.organization.OrganizationNotFoundException;
import bn.abb.task.mapper.OrganizationMapper;
import bn.abb.task.repository.OrganizationRepository;
import bn.abb.task.repository.UserRepository;
import bn.abb.task.service.OrganizationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrganizationServiceImpl implements OrganizationService {

    private final OrganizationRepository organizationRepository;
    private final OrganizationMapper organizationMapper;
    private final UserRepository userRepository;

    @Override
    public OrganizationResponseInfoDto create(OrganizationCreateRequestDto requestDto) {
        organizationRepository.findByName(requestDto.getName())
                .ifPresent(organization -> {
                    throw new NameMustBeUniqueException(requestDto.getName());
                });

        Organization organization = organizationMapper.requestToOrganization(requestDto);
        return organizationMapper.organizationToDto(organizationRepository.save(organization));
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void addUserToOrganization(AddUserToOrgDto registerOrgUser, Long id) {
        Organization organization = organizationFindById(id);
        User user = userFindById(registerOrgUser.getUserId());
        checkUserIsAlreadyInTheOrg(organization, registerOrgUser.getUserId());
        Set<User> users = organization.getUser();
        users.add(user);
        organization.setUser(users);
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    @Override
    public void removeUserFromOrganization(AddUserToOrgDto registerOrgUser, Long id) {
        Organization organization = organizationFindById(id);
        User user = userFindById(registerOrgUser.getUserId());
        Set<User> users = organization.getUser();
        users.remove(user);
        organization.setUser(users);
    }


    private Organization organizationFindById(Long id) {
        return organizationRepository.findById(id)
                .orElseThrow(() -> new OrganizationNotFoundException(id));
    }

    private User userFindById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
    }

    private void checkUserIsAlreadyInTheOrg(Organization organization, Long userId) {
        Set<User> test = organization.getUser().stream()
                .filter(u -> u.getId() == userId)
                .collect(Collectors.toSet());
        if (!test.isEmpty()) {
            throw new OrganizationAlreadyExistException(userId);
        }
    }
}
