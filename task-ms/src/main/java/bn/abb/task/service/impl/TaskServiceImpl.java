package bn.abb.task.service.impl;

import bn.abb.common.dto.GenericSearchDto;
import bn.abb.common.search.SearchSpecification;
import bn.abb.task.domain.Organization;
import bn.abb.task.domain.Task;
import bn.abb.task.dto.task.TaskCreateRequestDto;
import bn.abb.task.dto.task.TaskResponseDto;
import bn.abb.task.exception.NameMustBeUniqueException;
import bn.abb.task.exception.TaskNotFoundException;
import bn.abb.task.exception.organization.OrganizationNotFoundException;
import bn.abb.task.repository.OrganizationRepository;
import bn.abb.task.repository.TaskRepository;
import bn.abb.task.service.TaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final ModelMapper modelMapper;
    private final TaskRepository taskRepository;
    private final OrganizationRepository organizationRepository;


    @Override
    @Transactional
    public TaskResponseDto create(TaskCreateRequestDto requestDto) {
        Task task = modelMapper.map(requestDto, Task.class);
        taskRepository.findByTitle(requestDto.getTitle())
                .ifPresent(organization -> {
                    throw new NameMustBeUniqueException(requestDto.getTitle());
                });
        task.setTitle(requestDto.getTitle());
        task.setDescription(requestDto.getDescription());
        task.setDeadline(requestDto.getDeadline());
        task.setStatus(requestDto.getStatus());
        task.setOrganization(getOrganisation(requestDto.getOrganizationId()));
        task.setAssignee(task.getAssignee());
        taskRepository.save(task);
        return modelMapper.map(taskRepository.save(task), TaskResponseDto.class);
    }


    @Override
    @Transactional
    public void delete(Long id) {
        log.trace("Remove task with id {}", id);
        taskRepository.delete(taskRepository.findById(id)
                .orElseThrow(() -> new TaskNotFoundException(id)));
    }

    @Override
    public Page<TaskResponseDto> search(GenericSearchDto genericSearchDto, Pageable pageable) {
        return taskRepository
                .findAll(new SearchSpecification<>(genericSearchDto.getCriteria()), pageable)
                .map(task -> modelMapper.map(task, TaskResponseDto.class));
    }

    private Organization getOrganisation(Long organizationId) {
        return organizationRepository.findById(organizationId)
                .orElseThrow(OrganizationNotFoundException::new);
    }

}
