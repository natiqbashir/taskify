package bn.abb.task.web.rest;


import bn.abb.task.config.TestControllerConfig;
import bn.abb.task.domain.Organization;
import bn.abb.task.domain.User;
import bn.abb.task.dto.token.InteractionResponse;
import bn.abb.task.dto.user.RegisterRequestDto;
import bn.abb.task.repository.AuthorityRepository;
import bn.abb.task.repository.OrganizationRepository;
import bn.abb.task.repository.UserRepository;
import bn.abb.task.service.impl.AuthServiceImpl;
import bn.abb.task.service.security.UserDetailsServiceImpl;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@WithMockUser
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
@WebMvcTest(AuthController.class)
@Import({TestControllerConfig.class})
public class AuthControllerTest {

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private AuthServiceImpl authService;

    @Mock
    private UserRepository userRepository;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;


    @Mock
    private OrganizationRepository organizationRepository;

    @Mock
    private AuthorityRepository authRepository;

    private Organization organization;
    private RegisterRequestDto registerRequestDto;
    private User user;

    @Mock
    Set<User> mocUsers;

    private static final String DUMMY_STRING = "string";
    private static final String EMAIL = "test@test.com";
    private static final Long DUMMY_ID = 1L;
    private static final String ROLE_ADMIN = "ADMIN";


    @Before
    public void setup() {
        user = User.builder()
                .surname(DUMMY_STRING)
                .id(DUMMY_ID)
                .password(DUMMY_STRING)
                .email(EMAIL)
                .name(DUMMY_STRING)
                .organization(Organization.builder()
                        .name(DUMMY_STRING)
                        .build())
                .build();

        organization = Organization.builder()
                .name(DUMMY_STRING)
                .build();

        registerRequestDto = RegisterRequestDto.builder()
                .name(DUMMY_STRING)
                .password(DUMMY_STRING)
                .organizationName(DUMMY_STRING)
                .authority(Set.of("ADMIN"))
                .email(EMAIL)
                .build();
    }


    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    public void registerUser() {
        ResponseEntity<InteractionResponse> ok = ResponseEntity.ok(new InteractionResponse("User registered successfully!"));
        AuthController authController = new AuthController(authService);
        ResponseEntity<?> returnedValue = authController.registerUser(registerRequestDto);

        //Arrange
        when(authService.registerUser(registerRequestDto)).thenReturn(user);

        //Assert
        assertEquals(returnedValue, ok);


    }

}