package bn.abb.task.service;


import bn.abb.task.domain.Organization;
import bn.abb.task.domain.User;
import bn.abb.task.dto.organization.AddUserToOrgDto;
import bn.abb.task.dto.organization.OrganizationCreateRequestDto;
import bn.abb.task.dto.organization.OrganizationResponseInfoDto;
import bn.abb.task.exception.NameMustBeUniqueException;
import bn.abb.task.exception.organization.OrganizationAlreadyExistException;
import bn.abb.task.exception.organization.OrganizationNotFoundException;
import bn.abb.task.mapper.OrganizationMapper;
import bn.abb.task.repository.OrganizationRepository;
import bn.abb.task.repository.UserRepository;
import bn.abb.task.service.impl.OrganizationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrganizationServiceImplTest {

    private static final String NAME = "ABB";
    private static final Long DUMMY_ID = 1L;
    private static final String ORGANIZATION_NOT_FOUND_MESSAGE = "Organization with id 1 not Found.";
    private static final String ORGANIZATION_NAME_MUST_BE_UNIQUE = "This name called ABB has already been used.";
    private static final String USER_IS_ALREADY_IN_THE_GROUP = "User with id 1 is already in the organization.";

    @Mock
    private OrganizationRepository organizationRepository;

    @Mock
    private OrganizationMapper organizationMapper;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private OrganizationServiceImpl organizationService;

    @Mock
    Organization mockOrg;

    @Mock
    Set<User> mocUsers;

    private Organization organization;
    private OrganizationCreateRequestDto organizationRequest;
    private OrganizationResponseInfoDto orgResponse;
    private AddUserToOrgDto registerOrgUse;
    private User user;

    @BeforeEach
    void setUp() {
        user = getUser();

        organization = getOrganization();

        organizationRequest = getOrganizationRequestDto();

        orgResponse = getOrganizationResponseInfoDto();

        registerOrgUse = getRegisterOrgUse();

    }

    @Test
    public void givenRegisterOrganizationRequestDtoWhenCreateOrganizationThenSuccess() {
        //Arrange
        when(organizationMapper.requestToOrganization(organizationRequest)).thenReturn(organization);
        when(organizationRepository.save(organization)).thenReturn(organization);
        when(organizationMapper.organizationToDto(organization)).thenReturn(orgResponse);
        when(organizationRepository.findByName(NAME)).thenReturn(Optional.empty());

        //Act
        OrganizationResponseInfoDto result = organizationService.create(organizationRequest);

        //Assert
        assertThat(organizationRequest.getName()).isNotNull();
        assertThat(organization.getName()).isEqualTo(NAME);
        assertThat(result.getName()).isEqualTo(NAME);
        assertThat(result.getId()).isEqualTo(1L);
        verify(organizationRepository, times(1)).save(organization);
    }

    @Test
    public void givenRegisterOrganizationRequestDtoWhenCreateOrganizationThenException() {
        //Arrange
        when(organizationRepository.findByName(any())).thenReturn(Optional.of(organization));


        //Act & Assert
        assertThatThrownBy(() -> organizationService.create(organizationRequest))
                .isInstanceOf(NameMustBeUniqueException.class)
                .hasMessage(String.format(ORGANIZATION_NAME_MUST_BE_UNIQUE, NAME));
    }


    @Test
    void givenInValidIdAndValidServiceAddOrganizationUserWhenThenException() {
        //Arrange
        when(userRepository.findById(any())).thenReturn(Optional.of(user));
        when(organizationRepository.findById(any())).thenReturn(Optional.of(mockOrg));
        when(mockOrg.getUser()).thenReturn(Set.of(user));

        //Act & Assert
        assertThatThrownBy(() -> organizationService.addUserToOrganization(registerOrgUse, DUMMY_ID))
                .isInstanceOf(OrganizationAlreadyExistException.class)
                .hasMessage(String.format(USER_IS_ALREADY_IN_THE_GROUP, DUMMY_ID));
    }

    @Test
    void givenInValidIdAndValidServiceAddOrganizationUserWhenThenReturnOk() {
        //Arrange
        when(userRepository.findById(any())).thenReturn(Optional.of(user));
        when(organizationRepository.findById(any())).thenReturn(Optional.of(mockOrg));
        when(mockOrg.getUser()).thenReturn(mocUsers);
        when(mocUsers.add(any())).thenReturn(true);

        //Act
        organizationService.addUserToOrganization(registerOrgUse, DUMMY_ID);
    }


    @Test
    void givenInValidIdAndValidServiceDeleteOrganizationUserOrganizationIdWhenUpdateThenException() {
        //Arrange
        when(organizationRepository.findById(any())).thenReturn(Optional.empty());

        //Act & Assert
        assertThatThrownBy(() -> organizationService.removeUserFromOrganization(registerOrgUse, DUMMY_ID))
                .isInstanceOf(OrganizationNotFoundException.class)
                .hasMessage(String.format(ORGANIZATION_NOT_FOUND_MESSAGE, DUMMY_ID));
    }

    @Test
    void givenInValidIdAndValidServiceDeleteOrganizationUserWhenUpdateThenReturnOk() {
        //Arrange
        when(userRepository.findById(any())).thenReturn(Optional.of(user));
        when(organizationRepository.findById(any())).thenReturn(Optional.of(mockOrg));
        when(mockOrg.getUser()).thenReturn(mocUsers);
        when(mocUsers.remove(any())).thenReturn(true);

        //Act
        organizationService.removeUserFromOrganization(registerOrgUse, DUMMY_ID);
    }


    private OrganizationCreateRequestDto getOrganizationRequestDto() {
        return OrganizationCreateRequestDto.builder()
                .name(NAME)
                .build();
    }

    private Organization getOrganization() {
        return Organization.builder()
                .id(1L)
                .name(NAME)
                .user(Set.of(user))
                .build();
    }


    private User getUser() {
        return User.builder()
                .id(1L)
                .email("test@gmail.com")
                .build();
    }


    private AddUserToOrgDto getRegisterOrgUse() {
        return AddUserToOrgDto.builder()
                .userId(1L)
                .build();
    }

    private OrganizationResponseInfoDto getOrganizationResponseInfoDto() {
        return OrganizationResponseInfoDto.builder()
                .id(1L)
                .name(NAME)
                .build();
    }

}
