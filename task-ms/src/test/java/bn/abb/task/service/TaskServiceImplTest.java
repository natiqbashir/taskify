package bn.abb.task.service;

import bn.abb.common.dto.GenericSearchDto;
import bn.abb.common.exception.NotFoundException;
import bn.abb.common.search.SearchSpecification;
import bn.abb.task.domain.Organization;
import bn.abb.task.domain.Task;
import bn.abb.task.domain.enumeration.TaskStatus;
import bn.abb.task.dto.organization.OrganizationResponseInfoDto;
import bn.abb.task.dto.task.TaskCreateRequestDto;
import bn.abb.task.dto.task.TaskResponseDto;
import bn.abb.task.exception.NameMustBeUniqueException;
import bn.abb.task.repository.OrganizationRepository;
import bn.abb.task.repository.TaskRepository;
import bn.abb.task.repository.UserRepository;
import bn.abb.task.service.impl.TaskServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class TaskServiceImplTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final LocalDateTime DUMMY_DATETIME = LocalDateTime.parse("2015-02-20T06:30:00");
    private static final String TASK_TITLE_MUST_BE_UNIQUE = "This name called string has already been used.";

    @InjectMocks
    private TaskServiceImpl taskService;

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private OrganizationRepository organizationRepository;


    @Spy
    private ModelMapper modelMapper;

    @Captor
    private ArgumentCaptor<Task> csCaptor;

    private TaskCreateRequestDto taskRequestDto;

    private TaskResponseDto taskResponseDto;

    private Organization organization;

    private Task task;


    @BeforeEach
    void setUp() {
        taskRequestDto = getTaskRequestDto();

        taskResponseDto = getTaskResponseDto();

        organization = createOrganization();

        task = getTask();
    }

    @Test
    void givenTaskRequestDtoWhenCreateThenTaskResponseDto() {

        //Arrange
        TaskResponseDto taskResponseDto = modelMapper.map(task, TaskResponseDto.class);

        task.setOrganization(Organization.builder().id(DUMMY_ID).name(DUMMY_STRING).build());

        when(taskRepository.save(task)).thenReturn(task);
        when(organizationRepository.findById(any(Long.class))).thenReturn(Optional.of(organization));

        //Act
        TaskResponseDto result = taskService.create(taskRequestDto);

        //Assert
        verify(taskRepository, times(2)).save(csCaptor.capture());
        Task captured = csCaptor.getValue();

        assertEquals(task.getId(), captured.getId());
        assertEquals(task.getStatus(), captured.getStatus());
        assertEquals(task.getDeadline(), captured.getDeadline());
        assertEquals(task.getDescription(), captured.getDescription());
        assertEquals(task.getTitle(), captured.getTitle());
        assertEquals(task.getOrganization(), captured.getOrganization());
    }

    @Test
    public void givenRegisterOrganizationRequestDtoWhenCreateOrganizationThenException() {
        //Arrange
        when(taskRepository.findByTitle(any())).thenReturn(Optional.of(taskRepository));


        //Act & Assert
        assertThatThrownBy(() -> taskService.create(taskRequestDto))
                .isInstanceOf(NameMustBeUniqueException.class)
                .hasMessage(String.format(TASK_TITLE_MUST_BE_UNIQUE, DUMMY_STRING));
    }

    @Test
    void givenGenericSearchDtoWhenSearchThenExceptTaskResponseDto() {
        //Arrange
        when(taskRepository.findAll(any(SearchSpecification.class), any(Pageable.class)))
                .thenReturn(Page.empty());

        //Act
        taskService.search(new GenericSearchDto(), Pageable.unpaged());

        //Assert
        verify(taskRepository, times(1))
                .findAll(any(SearchSpecification.class), any(Pageable.class));
    }

    @Test
    void givenInValidTaskIdWhenDeleteThenException() {

        //Act & Assert
        assertThatThrownBy(() -> taskService.delete(DUMMY_ID)).isInstanceOf(NotFoundException.class);
    }

    @Test
    void givenValidTaskIdWhenDeleteThenOk() {

        when(taskRepository.findById(any(Long.class))).thenReturn(Optional.of(task));
        //Act
        taskService.delete(DUMMY_ID);

        //Verify
        verify(taskRepository, times(1)).delete(any(Task.class));
    }

    private Organization createOrganization() {
        return Organization.builder()
                .id(DUMMY_ID)
                .name(DUMMY_STRING)
                .build();
    }

    private TaskResponseDto getTaskResponseDto() {
        return TaskResponseDto
                .builder()
                .id(DUMMY_ID)
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATETIME)
                .organization(OrganizationResponseInfoDto.builder()
                        .id(DUMMY_ID)
                        .name(DUMMY_STRING)
                        .build())
                .build();
    }

    private Task getTask() {
        return Task
                .builder()
                .id(DUMMY_ID)
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATETIME)
                .organization(Organization.builder()
                        .id(DUMMY_ID)
                        .name(DUMMY_STRING)
                        .build())
                .build();
    }

    private TaskCreateRequestDto getTaskRequestDto() {
        return TaskCreateRequestDto
                .builder()
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATETIME)
                .organizationId(DUMMY_ID)
                .build();
    }


}
