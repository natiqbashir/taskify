package bn.abb.task.service;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import bn.abb.task.domain.Authority;
import bn.abb.task.domain.Organization;
import bn.abb.task.domain.User;
import bn.abb.task.domain.enumeration.AuthEnum;
import bn.abb.task.dto.user.LoginCreateRequestDto;
import bn.abb.task.dto.user.RegisterRequestDto;
import bn.abb.task.repository.AuthorityRepository;
import bn.abb.task.repository.OrganizationRepository;
import bn.abb.task.repository.UserRepository;
import bn.abb.task.security.JwtUtils;
import bn.abb.task.service.impl.AuthServiceImpl;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;

@ExtendWith(MockitoExtension.class)
public class AuthServiceImplTest {


    @Mock
    private UserRepository userRepository;
    @Mock
    private AuthorityRepository authRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private JwtUtils jwtUtils;
    @Mock
    private OrganizationRepository organizationRep;
    @InjectMocks
    private AuthServiceImpl authService;

    @Mock
    AuthenticationManager authenticationManager;

    private Set<User> mocUser;

    private User user;
    private Organization organization;
    private RegisterRequestDto registerRequestDto;
    private LoginCreateRequestDto createRequestDto;

    private static final String DUMMY_STRING = "string";
    private static final String EMAIL = "test@test.com";
    private static final Long DUMMY_ID = 1L;
    private static final String ROLE_ADMIN = "ADMIN";

    @BeforeEach
    public void setUp() {
        user = User.builder()
                .surname(DUMMY_STRING)
                .id(DUMMY_ID)
                .password(DUMMY_STRING)
                .email(EMAIL)
                .name(DUMMY_STRING)
                .organization(Organization.builder()
                        .name(DUMMY_STRING)
                        .build())
                .build();
        organization = Organization.builder()
                .name(DUMMY_STRING)
                .build();
        registerRequestDto = RegisterRequestDto.builder()
                .name(DUMMY_STRING)
                .password(DUMMY_STRING)
                .organizationName(DUMMY_STRING)
                .authority(Set.of(ROLE_ADMIN))
                .email(DUMMY_STRING)
                .build();
        createRequestDto = LoginCreateRequestDto.builder()
                .email(EMAIL)
                .password(DUMMY_STRING)
                .build();

    }


    @Test
    public void registerUser() {
        when(passwordEncoder.encode(any())).thenReturn(DUMMY_STRING);
        when(userRepository.existsByEmail(any())).thenReturn(false);
        when(authRepository.findByName(any())).thenReturn(Optional.of(Authority.builder()
                .name(AuthEnum.ADMIN)
                .build()));
        when(userRepository.save(any())).thenReturn(user);
        when(organizationRep.save(any())).thenReturn(organization);

        User user = authService.registerUser(registerRequestDto);

        assertThat(user.getEmail()).isEqualTo(DUMMY_STRING);

    }

    public void login(){

    }
}


