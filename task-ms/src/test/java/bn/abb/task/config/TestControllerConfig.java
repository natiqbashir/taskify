package bn.abb.task.config;


import bn.abb.task.security.AuthEntryPointJwt;
import bn.abb.task.security.JwtUtils;
import bn.abb.task.service.security.UserDetailsImpl;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class TestControllerConfig {
    @Bean
    public UserDetailsImpl controllerService() {
        return new UserDetailsImpl(null, null, null, null);
    }

    @Bean
    public AuthEntryPointJwt bean() {
        return new AuthEntryPointJwt();
    }

    @Bean
    public JwtUtils jwtUtils(){
        return new JwtUtils();
    }
}